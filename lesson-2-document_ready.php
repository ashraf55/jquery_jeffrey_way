<!doctype html>
<html lang="">
    <head>
      <title>Jquery For Begineers</title>
      <!-- <script src="jquery-3.1.1.js"></script> -->
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    	<style>
    		ul li {
    			color: red;
    		}
    		.emphasis{
    			color: green;
                font-weight: bold;
    		}
    		.emphasis2{
    			color: blue;
    		}
    	</style>
      <script>
        // $(function() { //document.ready function and delcaring function this way is exactly the same.
        //     $('ul li:last-child').addClass('emphasis');
        // });

        $(document).ready(function() {
            $('ul li:first-child').addClass('emphasis');
        });
      </script>
    </head>
    <body>
        <ul>
        	<li>Hello</li>
        	<li>Dear</li>
        	<li>Kitty</li>
        	<li>How</li>
        	<li>Are</li>
        	<li>You ?</li>
        </ul>
    </body>
</html>
